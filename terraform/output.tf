output "rds_endpoint" {
  value = "${aws_rds_cluster_instance.mediawiki_db.endpoint}"
}

output "mediawiki_public_ip" {
  value = "${aws_instance.mediawiki.public_ip}"
}

output "elb_dns_name" {
  value = "${aws_elb.mediawiki_elb.dns_name}"
}
