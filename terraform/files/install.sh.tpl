#!/bin/bash
sudo apt-get update -y 
sudo apt-get install awscli git wget apache2 php php-mysql libapache2-mod-php php-xml php-mbstring php-apcu php-intl imagemagick inkscape php-cli php-curl php-gd -y
sudo systemctl enable apache2.service
sudo service apache2 reload
cd /tmp/
wget https://releases.wikimedia.org/mediawiki/${VERSION}/mediawiki-${PATCH}.tar.gz
tar -xvzf /tmp/mediawiki-*.tar.gz
sudo mkdir /var/lib/mediawiki
sudo mv mediawiki-*/* /var/lib/mediawiki
sudo ln -s /var/lib/mediawiki /var/www/html/mediawiki
/usr/bin/aws s3 sync s3://${bucket}/config/LocalSettings.php /var/www/mediawiki/
sudo phpenmod mbstring
sudo phpenmod xml
sudo systemctl restart apache2.service
sudo mv /tmp/LocalSettings.php /var/lib/mediawiki/
sudo chown www-data:www-data /var/lib/mediawiki/LocalSettings.php
/bin/touch /var/spool/cron/root
sudo /bin/echo '*/5 * * * * aws s3 cp s3://${bucket}/config/ /var/www/mediawiki >> /var/spool/cron/root