provider "aws" {
  version = "2.7.0"
  profile = var.aws_profile
  region  = var.aws_region
}

terraform {
  backend "s3" {
    bucket = "mediawiki-terraform-state-1"
    key    = "global/terraform.tfstate"
    region = "us-east-1"
    #dynamodb_table = "great-name-locks-2"
  }
}

#----IAM Role-------
#s3_access
resource "aws_iam_instance_profile" "s3_access_profile" {
  name = "s3_access"
  role = aws_iam_role.s3_access_role.name
}

resource "aws_iam_role_policy" "s3_access_policy" {
  name = "s3_access_policy"
  role = aws_iam_role.s3_access_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:*",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role" "s3_access_role" {
  name = "s3_access_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
  {
    "Action": "sts:AssumeRole",
    "Principal": {
       "Service": "ec2.amazonaws.com"
   },
    "Effect": "Allow",
    "Sid": ""
   }
  ]
}
EOF
}

#----------vpc ------
resource "aws_vpc" "mediawiki_vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "${var.tag_name}_vpc"
  }
}

#internet gateway

resource "aws_internet_gateway" "mediawiki_internet_gateway" {
  vpc_id = aws_vpc.mediawiki_vpc.id

  tags = {
    Name = "${var.tag_name}_igw"
  }
}

# Route table

resource "aws_route_table" "mediawiki_public_rt" {
  vpc_id = aws_vpc.mediawiki_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.mediawiki_internet_gateway.id
  }

  tags = {
    Name = "${var.tag_name}_public"
  }
}

#resource "aws_default_route_table" "mediawiki_private_rt" {
#  default_route_table_id = aws_vpc.mediawiki_vpc.default_route_table_id

#  tags = {
#    Name = "${var.tag_name}_private"
#  }
#}



####subnets

resource "aws_subnet" "mediawiki_public1_subnet" {
  vpc_id                  = aws_vpc.mediawiki_vpc.id
  cidr_block              = var.cidrs["public1"]
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "${var.tag_name}_public1"
  }
}

resource "aws_subnet" "mediawiki_public2_subnet" {
  vpc_id                  = aws_vpc.mediawiki_vpc.id
  cidr_block              = var.cidrs["public2"]
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "${var.tag_name}_public2"
  }
}
resource "aws_subnet" "mediawiki_rds1_subnet" {
  vpc_id                  = aws_vpc.mediawiki_vpc.id
  cidr_block              = var.cidrs["rds1"]
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "${var.tag_name}_rds1"
  }
}

resource "aws_subnet" "mediawiki_rds2_subnet" {
  vpc_id                  = aws_vpc.mediawiki_vpc.id
  cidr_block              = var.cidrs["rds2"]
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "${var.tag_name}_rds2"
  }
}

#rds subnet group

resource "aws_db_subnet_group" "mediawiki_rds_subnetgroup" {
  name = "mediawiki_rds_subnetgroup"

  subnet_ids = [aws_subnet.mediawiki_rds1_subnet.id,
    aws_subnet.mediawiki_rds2_subnet.id,
  ]

  tags = {
    Name = "${var.tag_name}_rds_sng"
  }
}

#Subnet Associations

resource "aws_route_table_association" "mediawiki_public1_assoc" {
  subnet_id      = aws_subnet.mediawiki_public1_subnet.id
  route_table_id = aws_route_table.mediawiki_public_rt.id
}

resource "aws_route_table_association" "mediawiki_public2_assoc" {
  subnet_id      = "${aws_subnet.mediawiki_public2_subnet.id}"
  route_table_id = aws_route_table.mediawiki_public_rt.id
}

#public Security group

resource "aws_security_group" "mediawiki_public_sg" {
  name        = "mediawiki_public_sg"
  description = "Used for public instance for public access"
  vpc_id      = aws_vpc.mediawiki_vpc.id

  #http
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "mediawiki_elb_sg" {
  name        = "mediawiki_elb_sg"
  description = "Used for aelastic load balancer for public access"
  vpc_id      = "${aws_vpc.mediawiki_vpc.id}"

  #http
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_security_group" "mediawiki_rds_sg" {
  name        = "mediawiki_rds_sg"
  description = "Used for rds instances"
  vpc_id      = aws_vpc.mediawiki_vpc.id

  #SQL access from public/private security groups
  ingress {
    from_port = 3306
    to_port   = 3306
    protocol  = "tcp"

    security_groups = [
      aws_security_group.mediawiki_public_sg.id,
    ]
  }
}

#vpc Endpointfor s3

resource "aws_vpc_endpoint" "mediawiki_private-s3_endpoint" {
  vpc_id       = "${aws_vpc.mediawiki_vpc.id}"
  service_name = "com.amazonaws.${var.aws_region}.s3"

  route_table_ids = ["${aws_vpc.mediawiki_vpc.main_route_table_id}",
    "${aws_route_table.mediawiki_public_rt.id}",
  ]

  policy = <<POLICY
{
  "Statement": [
   {
     "Action": "*",
     "Effect": "Allow",
     "Resource": "*",
     "Principal":  "*"
   }
  ]
}
POLICY
}

resource "aws_rds_cluster" "mediawiki_cluster" {
  cluster_identifier     = var.RDS_cluster_identifier
  engine                 = var.RDS_engine
  engine_version         = var.RDS_engine_version
  database_name          = var.dbname
  master_username        = var.dbuser
  master_password        = var.dbpassword
  db_subnet_group_name   = aws_db_subnet_group.mediawiki_rds_subnetgroup.name
  vpc_security_group_ids = [aws_security_group.mediawiki_rds_sg.id]
  skip_final_snapshot    = true
}

resource "aws_rds_cluster_instance" "mediawiki_db" {
  cluster_identifier = aws_rds_cluster.mediawiki_cluster.id
  engine             = var.RDS_engine
  instance_class     = var.db_instance_type
  identifier         = var.RDS_instance_identifier
}

#key pair

resource "aws_key_pair" "mediawiki_auth" {
  key_name   = var.key_name
  public_key = file(var.public_key_path)
}

data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

#data "template_file" "mediawiki_setup" {
#  template = file("files/install.sh.tpl")
#  vars = {
#    VERSION         = var.mediawiki_version
#    PATCH           = var.mediawiki_package
#    bucket          = var.s3_bucket
#  }
#}
data "template_file" "config_setup" {
  template = file("files/LocalSettings.php.tpl")
  vars = {
    lb_url            = aws_elb.mediawiki_elb.dns_name
    database_endpoint = aws_rds_cluster_instance.mediawiki_db.endpoint
  }
}



resource "null_resource" "local" {
  provisioner "local-exec" {
    command = "echo \"${data.template_file.config_setup.rendered}\" > ../ansible/roles/mediawiki/templates/LocalSettings.php"
  }
}

#---------- s3 upload -------------------

#resource "aws_s3_bucket_object" "s3_upload" {
#  depends_on = [ 
#     null_resource.local
#   ]
#  bucket = var.s3_bucket
#  key    = "config/LocalSettings.php"
#  acl    = "private"  # or can be "public-read"
#  source = "files/LocalSettings.php"
#  etag   = filemd5("files/LocalSettings.php")

#}
resource "aws_instance" "mediawiki" {
  instance_type               = var.dev_instance_type
  ami                         = data.aws_ami.ubuntu.id
  associate_public_ip_address = "true"
  tags = {
    Name = "${var.tag_name}_dev"
  }
  key_name               = aws_key_pair.mediawiki_auth.id
  vpc_security_group_ids = [aws_security_group.mediawiki_public_sg.id]
  subnet_id              = aws_subnet.mediawiki_public1_subnet.id
  provisioner "local-exec" {
    command = <<EOD
  cat <<EOF> ../ansible/aws_hosts
[mediawiki]
${aws_instance.mediawiki.public_ip}
[mediawiki:vars]
elb_endpoint=${aws_elb.mediawiki_elb.dns_name}
rds_endpoint=${aws_rds_cluster_instance.mediawiki_db.endpoint}
EOF
EOD
  }

  provisioner "local-exec" {
    command = "aws ec2 wait instance-status-ok --instance-ids ${aws_instance.mediawiki.id} --profile ${var.aws_profile}  && cd ../ansible && ansible-playbook -i aws_hosts mediawiki.yaml"
  }
}

#-----Load balancer-----

resource "aws_elb" "mediawiki_elb" {
  name = "${var.domain_name}-elb"

  subnets = [aws_subnet.mediawiki_public1_subnet.id,
    aws_subnet.mediawiki_public2_subnet.id,
  ]

  security_groups = [aws_security_group.mediawiki_elb_sg.id]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = var.elb_healthy_threshold
    unhealthy_threshold = var.elb_unhealthy_threshold
    timeout             = var.elb_timeout
    target              = "TCP:80"
    interval            = var.elb_interval
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "${var.tag_name}_${var.domain_name}-elb"
  }
}

resource "aws_lb_cookie_stickiness_policy" "mediawiki_lb_policy" {
  name                     = "mw-lb-policy"
  load_balancer            = "${aws_elb.mediawiki_elb.id}"
  lb_port                  = 80
  cookie_expiration_period = 600
}

############## Golden ami ###########################

# random ami id

resource "random_id" "golden_ami" {
  byte_length = 4
}

#  AMI

resource "aws_ami_from_instance" "mediawiki_golden" {
  depends_on = [
    aws_instance.mediawiki
  ]
  name               = "media-wiki-${random_id.golden_ami.b64_url}"
  source_instance_id = aws_instance.mediawiki.id
  #provisioner "local-exec" {
  #  command = <<EOT
  #cat <<EOF > userdata
  ##!/bin/bash
  #sudo dpkg --configure -a
  #sudo /bin/touch /var/spool/cron/root
  #sudo /bin/echo '*/5 * * * * aws s3 cp s3://${var.s3_bucket}/config/LocalSettings.php /var/lib/mediawiki >> /var/spool/cron/root'
  #EOF
  #EOT
  #}
}

#delete media instance
#resource "null_resource" "cluster" {

#  provisioner "local-exec" {
#  command = "terraform destroy -target aws_instance.${aws_instance.mediawiki.id} -input=false -auto-approve"
#  }
#  depends_on = [
#    aws_ami_from_instance.mediawiki_golden,
#  ]
#}

#---------- Launch Configuration ---------

resource "aws_launch_configuration" "mediawiki_lc" {
  name_prefix                 = "mediawiki_lc-"
  image_id                    = aws_ami_from_instance.mediawiki_golden.id
  instance_type               = var.lc_instance_type
  security_groups             = [aws_security_group.mediawiki_public_sg.id]
  iam_instance_profile        = aws_iam_instance_profile.s3_access_profile.id
  key_name                    = aws_key_pair.mediawiki_auth.id
  user_data                   = file("userdata")
  associate_public_ip_address = "false"



  lifecycle {
    create_before_destroy = true
  }
}

#------- ASG ------

resource "aws_autoscaling_group" "mediawiki_asg" {
  name                      = "asg-${aws_launch_configuration.mediawiki_lc.id}"
  max_size                  = var.asg_max
  min_size                  = var.asg_min
  health_check_grace_period = var.asg_grace
  health_check_type         = var.asg_hct
  desired_capacity          = var.asg_cap
  force_delete              = true
  load_balancers            = [aws_elb.mediawiki_elb.id]
  termination_policies      = ["OldestLaunchConfiguration"]

  vpc_zone_identifier = [aws_subnet.mediawiki_public1_subnet.id,
    aws_subnet.mediawiki_public2_subnet.id,
  ]

  launch_configuration = aws_launch_configuration.mediawiki_lc.name

  tag {
    key                 = "Name"
    value               = "mediawiki_asg-instance"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}