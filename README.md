This repository will help you deploy Mediawiki on AWS in fully automated using Terraforma, AWS cli and Ansible.
Terraform is responsible for provisioning infrastructure on AWS while Ansible helps install Mediawiki.

Architecture:

Traffic arrives on load balancer and get routed to web servers in public subnet. This web servers are part of auto scalling group. This web servers can make calls to RDS aurora mysql cluster. After Running this code terraform will create all aws infra and ansible will install mediawiki server. After server installation this terraform will create a golden image and create auto scalling and launch configuration with this new gloden image.

Steps to execute Terrform Scripts: 

Prerequists:
You will require AWS account and Iam user with admin access and credential keys. Configure this credenntial on local laptop or save this in jenkins credential manager to access during CICD.
Generet a ssh key pair and we will require public key to create a AWS ssh key for instance login.
Make sure below tools are installed on the server where you want to Run this code. This can be your local laptop or jenkins server. 
1. Terraform 0.13
2. Awscli
3. ansible 2.7

To run from local laptop.

1. Clone this repo.
2. cd terraform 
3. terraform plan 
4. terraform apply

To Destroy created cluster.
5. terraform destroy

CICD:

1. Added Git and aws access key/secrets to jenkins credential manager. This will require during cloning repository and deploying this on AWS. 
2. Create a jenkins pipeline job.
3. Select pipeline script from SCM and Scipt path "jenkinsfile"


